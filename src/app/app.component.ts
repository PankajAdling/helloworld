import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hello-world';

  MyId="testId";

  public isError=false;
  public isSpecial=true;
  public successClass="text-success";

  public isDisable=true;


  messageClass={
    "text-success":!this.isError,
    "text-error": this.isError,
    "text-special":this.isSpecial
  }
}
